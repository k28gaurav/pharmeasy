package com.pharmeasy.pharma;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.couchbase.lite.Document;
import com.couchbase.lite.LiveQuery;
import com.couchbase.lite.QueryEnumerator;

import java.util.List;

/**
 * Created by kumargaurav on 5/9/16.
 */
public class MedicinePagerAdapter  extends FragmentStatePagerAdapter {

    private LiveQuery query;
    private QueryEnumerator enumerator;
    Context _context;

    public MedicinePagerAdapter(MedicineDetailActivity medicineDetailActivity, FragmentManager supportFragmentManager, LiveQuery liveQuery) {
        super(supportFragmentManager);
        query = liveQuery;
        _context = medicineDetailActivity;
        query.addChangeListener(new LiveQuery.ChangeListener() {
            @Override
            public void changed(final LiveQuery.ChangeEvent changeEvent) {
                ((Activity) MedicinePagerAdapter.this._context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        enumerator = changeEvent.getRows();
                        notifyDataSetChanged();
                    }
                });
            }
        });
        query.start();
    }

    @Override
    public Fragment getItem(int position) {
        final Document task = (Document) getItemAtPosition(position);
        return MedicinePagerFragment.newInstance(task);
    }

    @Override
    public int getCount() {
        return enumerator != null ? enumerator.getCount() : 0;
    }

    public Object getItemAtPosition(int i) {
        return enumerator != null ? enumerator.getRow(i).getDocument() : null;
    }

}
