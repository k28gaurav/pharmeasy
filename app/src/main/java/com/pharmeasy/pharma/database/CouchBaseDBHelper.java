package com.pharmeasy.pharma.database;

import android.content.Context;
import android.provider.ContactsContract;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.LiveQuery;
import com.couchbase.lite.Manager;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.View;
import com.couchbase.lite.android.AndroidContext;
import com.couchbase.lite.replicator.Replication;
import com.couchbase.lite.util.Log;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by kumargaurav on 5/8/16.
 */
public class CouchBaseDBHelper implements Replication.ChangeListener {
    private static CouchBaseDBHelper instance;
    Database database;
    Manager manager;
    String DB_NAME = "pherma";
    Context ctx = null;
    public String TAG = "CouchBaseDBHelper";

    public CouchBaseDBHelper(Context context) {
        ctx = context;
        try {
            try {
                database = getDatabaseInstance();
                manager = getManagerInstance();
                startReplications();
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
    }

    public void addDocument(String docId, Map<String, Object> data) {
        Document document = database.getDocument(docId);
        try {
            document.putProperties(data);
        } catch (CouchbaseLiteException e) {

        }
    }

    private URL createSyncURL() {
        URL syncURL = null;
        String host = "";
        String port = "";
        try {
            syncURL = new URL("http://" + host + ":" + port + "/" + DB_NAME);
        } catch (MalformedURLException me) {
            me.printStackTrace();
        }
        catch(Exception e)
        {
        }
        return syncURL;
    }

    private void startReplications() {
        try {
            if (database == null) {
                Log.e(TAG, "getDatabase Instance");
                database = getDatabaseInstance();
            }
            if (database != null && !database.isOpen()) {
                database.open();
            }
            /*Replication pushReplication = database.createPushReplication(this.createSyncURL());
            pushReplication.setContinuous(true);
            pushReplication.start();*/
        } catch (Exception e) {
            Log.e(TAG, "in CouchDB in start replication" + database + "Message" + e.getMessage());
        }
    }

    @Override
    public void changed(Replication.ChangeEvent event) {
        try {
            Replication replication = event.getSource();
            Log.d(TAG, "Replication : " + replication + " changed.");
            if (!replication.isRunning()) {
                String msg = String.format("Replicator %s not running", replication);
                Log.d(TAG, msg);
            } else {
                int processed = replication.getCompletedChangesCount();
                int total = replication.getChangesCount();
                String msg = String.format("Replicator processed %d / %d", processed, total);
                Log.d(TAG, msg);
            }

            if (event.getError() != null) {
                showError("Sync error", event.getError());
            }
        } catch (Exception e) {
        }
    }

    public void showError(final String errorMessage, final Throwable throwable) {
        try {
            String msg = String.format("%s: %s", errorMessage, throwable);
            Log.e(TAG, msg, throwable);
//        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
        }
    }

    public Database getDatabaseInstance() throws CouchbaseLiteException {
        try {
            if ((database == null) & (manager != null)) {
                database = manager.getDatabase(DB_NAME);
                Log.e(TAG, "Database instance: " + database);
            }
        } catch (Exception e) {
        }
        return database;
    }

    public Manager getManagerInstance() throws IOException {
        try {
            if (manager == null) {
                manager = new Manager(new AndroidContext(ctx), Manager.DEFAULT_OPTIONS);
            }
        } catch (Exception e) {
        }
        return manager;
    }
}
