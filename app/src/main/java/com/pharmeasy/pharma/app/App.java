package com.pharmeasy.pharma.app;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.pharmeasy.pharma.rest.RestClient;

/**
 * Created by kumargaurav on 12/8/15.
 */
public class App extends Application
{
    private static RestClient restClient;

    @Override
    public void onCreate() {
        super.onCreate();

    }

    public static RestClient getRestClient()
    {
        try
        {
        restClient = new RestClient();

        }
        catch(Exception e)
        {
        }
        return restClient;
    }
}

