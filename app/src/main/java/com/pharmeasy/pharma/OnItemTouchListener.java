package com.pharmeasy.pharma;

import android.view.View;

/**
 * Created by kumargaurav on 5/9/16.
 */
public interface OnItemTouchListener {
    public void onCardViewTap(View view, int position);
}
