package com.pharmeasy.pharma;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.couchbase.lite.Document;
import com.pharmeasy.pharma.model.Result;

import java.io.File;
import java.io.Serializable;

public class MedicinePagerFragment extends Fragment {
    private static final String IMAGE_DATA_EXTRA = "extra_image_data";
    private static MedicinePagerFragment imagePagerFragment;
    private static Bundle bundle;
    private Result result;
    public TextView tvName, tvLabel, tvManufacturer , tvAvailable ,tvMRP;
    public ImageView ivMedicine;

    public static MedicinePagerFragment newInstance(Document document) {
        if (imagePagerFragment != null) {
            imagePagerFragment = null;
            bundle = null;
        }
        imagePagerFragment = new MedicinePagerFragment();
        Result result = new Result();
        result.setName((String) document.getProperty("name"));
        result.setLabel((String) document.getProperty("label"));
        result.setManufacturer(document.getProperty("manufacturer"));
        result.setAvailable((Boolean) document.getProperty("available"));
        result.setMrp((Double) document.getProperty("mrp"));
        bundle = new Bundle();
        bundle.putSerializable(IMAGE_DATA_EXTRA, result);
        imagePagerFragment.setArguments(bundle);
        return imagePagerFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        result = getArguments() != null ? (Result) getArguments().getSerializable(IMAGE_DATA_EXTRA) : null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View itemLayoutView = inflater.inflate(R.layout.layout, container, false);
        tvName = (TextView) itemLayoutView.findViewById(R.id.tv_name_value);
        tvLabel = (TextView) itemLayoutView.findViewById(R.id.tv_label_value);
        tvManufacturer = (TextView) itemLayoutView.findViewById(R.id.tv_manufacturer_value);
        tvAvailable = (TextView) itemLayoutView.findViewById(R.id.tv_available_value);
        tvMRP = (TextView) itemLayoutView.findViewById(R.id.tv_mrp_value);
        return itemLayoutView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inflateData(result);
    }

    private void inflateData(Result result) {
        try {
            tvName.setText(result.getName());
            tvLabel.setText(result.getLabel());
            tvManufacturer.setText(result.getManufacturer().toString());
            tvAvailable.setText(result.getAvailable().toString());
            tvMRP.setText(String.valueOf(result.getMrp()));
        } catch (Exception e) {
            Log.e("MedicinePagerFragment", "onBindViewHolder" + e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (tvName != null) {
            imagePagerFragment = null;
            bundle = null;
        }
    }

}
