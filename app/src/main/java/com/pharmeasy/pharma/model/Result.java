package com.pharmeasy.pharma.model;

/**
 * Created by kumargaurav on 5/8/16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable {

    @SerializedName("packSizeLabel")
    @Expose
    private Object packSizeLabel;
    @SerializedName("oPrice")
    @Expose
    private double oPrice;
    @SerializedName("mrp")
    @Expose
    private double mrp;
    @SerializedName("imgUrl")
    @Expose
    private Object imgUrl;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("drug_form")
    @Expose
    private Object drugForm;
    @SerializedName("uip")
    @Expose
    private int uip;
    @SerializedName("uPrice")
    @Expose
    private double uPrice;
    @SerializedName("available")
    @Expose
    private Boolean available;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("manufacturer")
    @Expose
    private Object manufacturer;
    @SerializedName("prescriptionRequired")
    @Expose
    private Object prescriptionRequired;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("slug")
    @Expose
    private Object slug;
    @SerializedName("packForm")
    @Expose
    private Object packForm;
    @SerializedName("form")
    @Expose
    private Object form;
    @SerializedName("discountPerc")
    @Expose
    private Integer discountPerc;
    @SerializedName("pForm")
    @Expose
    private Object pForm;
    @SerializedName("productsForBrand")
    @Expose
    private Object productsForBrand;
    @SerializedName("meta")
    @Expose
    private String meta;
    @SerializedName("hkpDrugCode")
    @Expose
    private Object hkpDrugCode;
    @SerializedName("generics")
    @Expose
    private Object generics;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("packSize")
    @Expose
    private Object packSize;
    @SerializedName("mfId")
    @Expose
    private Object mfId;
    @SerializedName("su")
    @Expose
    private Integer su;
    @SerializedName("mappedPForm")
    @Expose
    private Object mappedPForm;

    /**
     *
     * @return
     * The packSizeLabel
     */
    public Object getPackSizeLabel() {
        return packSizeLabel;
    }

    /**
     *
     * @param packSizeLabel
     * The packSizeLabel
     */
    public void setPackSizeLabel(Object packSizeLabel) {
        this.packSizeLabel = packSizeLabel;
    }

    /**
     *
     * @return
     * The oPrice
     */
    public double getOPrice() {
        return oPrice;
    }

    /**
     *
     * @param oPrice
     * The oPrice
     */
    public void setOPrice(double oPrice) {
        this.oPrice = oPrice;
    }

    /**
     *
     * @return
     * The mrp
     */
    public double getMrp() {
        return mrp;
    }

    /**
     *
     * @param mrp
     * The mrp
     */
    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    /**
     *
     * @return
     * The imgUrl
     */
    public Object getImgUrl() {
        return imgUrl;
    }

    /**
     *
     * @param imgUrl
     * The imgUrl
     */
    public void setImgUrl(Object imgUrl) {
        this.imgUrl = imgUrl;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The drugForm
     */
    public Object getDrugForm() {
        return drugForm;
    }

    /**
     *
     * @param drugForm
     * The drug_form
     */
    public void setDrugForm(Object drugForm) {
        this.drugForm = drugForm;
    }

    /**
     *
     * @return
     * The uip
     */
    public int getUip() {
        return uip;
    }

    /**
     *
     * @param uip
     * The uip
     */
    public void setUip(int uip) {
        this.uip = uip;
    }

    /**
     *
     * @return
     * The uPrice
     */
    public double getUPrice() {
        return uPrice;
    }

    /**
     *
     * @param uPrice
     * The uPrice
     */
    public void setUPrice(double uPrice) {
        this.uPrice = uPrice;
    }

    /**
     *
     * @return
     * The available
     */
    public Boolean getAvailable() {
        return available;
    }

    /**
     *
     * @param available
     * The available
     */
    public void setAvailable(Boolean available) {
        this.available = available;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The manufacturer
     */
    public Object getManufacturer() {
        return manufacturer;
    }

    /**
     *
     * @param manufacturer
     * The manufacturer
     */
    public void setManufacturer(Object manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     *
     * @return
     * The prescriptionRequired
     */
    public Object getPrescriptionRequired() {
        return prescriptionRequired;
    }

    /**
     *
     * @param prescriptionRequired
     * The prescriptionRequired
     */
    public void setPrescriptionRequired(Object prescriptionRequired) {
        this.prescriptionRequired = prescriptionRequired;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The slug
     */
    public Object getSlug() {
        return slug;
    }

    /**
     *
     * @param slug
     * The slug
     */
    public void setSlug(Object slug) {
        this.slug = slug;
    }

    /**
     *
     * @return
     * The packForm
     */
    public Object getPackForm() {
        return packForm;
    }

    /**
     *
     * @param packForm
     * The packForm
     */
    public void setPackForm(Object packForm) {
        this.packForm = packForm;
    }

    /**
     *
     * @return
     * The form
     */
    public Object getForm() {
        return form;
    }

    /**
     *
     * @param form
     * The form
     */
    public void setForm(Object form) {
        this.form = form;
    }

    /**
     *
     * @return
     * The discountPerc
     */
    public Integer getDiscountPerc() {
        return discountPerc;
    }

    /**
     *
     * @param discountPerc
     * The discountPerc
     */
    public void setDiscountPerc(Integer discountPerc) {
        this.discountPerc = discountPerc;
    }

    /**
     *
     * @return
     * The pForm
     */
    public Object getPForm() {
        return pForm;
    }

    /**
     *
     * @param pForm
     * The pForm
     */
    public void setPForm(Object pForm) {
        this.pForm = pForm;
    }

    /**
     *
     * @return
     * The productsForBrand
     */
    public Object getProductsForBrand() {
        return productsForBrand;
    }

    /**
     *
     * @param productsForBrand
     * The productsForBrand
     */
    public void setProductsForBrand(Object productsForBrand) {
        this.productsForBrand = productsForBrand;
    }

    /**
     *
     * @return
     * The meta
     */
    public String getMeta() {
        return meta;
    }

    /**
     *
     * @param meta
     * The meta
     */
    public void setMeta(String meta) {
        this.meta = meta;
    }

    /**
     *
     * @return
     * The hkpDrugCode
     */
    public Object getHkpDrugCode() {
        return hkpDrugCode;
    }

    /**
     *
     * @param hkpDrugCode
     * The hkpDrugCode
     */
    public void setHkpDrugCode(Object hkpDrugCode) {
        this.hkpDrugCode = hkpDrugCode;
    }

    /**
     *
     * @return
     * The generics
     */
    public Object getGenerics() {
        return generics;
    }

    /**
     *
     * @param generics
     * The generics
     */
    public void setGenerics(Object generics) {
        this.generics = generics;
    }

    /**
     *
     * @return
     * The label
     */
    public String getLabel() {
        return label;
    }

    /**
     *
     * @param label
     * The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     *
     * @return
     * The packSize
     */
    public Object getPackSize() {
        return packSize;
    }

    /**
     *
     * @param packSize
     * The packSize
     */
    public void setPackSize(Object packSize) {
        this.packSize = packSize;
    }

    /**
     *
     * @return
     * The mfId
     */
    public Object getMfId() {
        return mfId;
    }

    /**
     *
     * @param mfId
     * The mfId
     */
    public void setMfId(Object mfId) {
        this.mfId = mfId;
    }

    /**
     *
     * @return
     * The su
     */
    public Integer getSu() {
        return su;
    }

    /**
     *
     * @param su
     * The su
     */
    public void setSu(Integer su) {
        this.su = su;
    }

    /**
     *
     * @return
     * The mappedPForm
     */
    public Object getMappedPForm() {
        return mappedPForm;
    }

    /**
     *
     * @param mappedPForm
     * The mappedPForm
     */
    public void setMappedPForm(Object mappedPForm) {
        this.mappedPForm = mappedPForm;
    }

}
