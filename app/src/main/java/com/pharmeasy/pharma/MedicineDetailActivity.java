package com.pharmeasy.pharma;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.LiveQuery;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.pharmeasy.pharma.app.App;
import com.pharmeasy.pharma.database.CouchBaseDBHelper;
import com.pharmeasy.pharma.rest.model.ListParser;
import com.pharmeasy.pharma.rest.service.Medicine;

import java.util.HashMap;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MedicineDetailActivity extends FragmentActivity {

    private String url ="api/v1/search/autocomplete";
    private Map<String, String> data;
    private Map<String, Object> property;
    private CouchBaseDBHelper db;
    private ProgressDialog progressDialog;
    TextView text;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_detail);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        text = (TextView)findViewById(R.id.msg);

        db = new CouchBaseDBHelper(getApplicationContext());
        property = new HashMap<>();
        data = new HashMap<>();
        if (getCount() == 0) {
            fetchAndStoreData();
        }
        else {
            initializeView();
        }
    }

    private void initializeView() {
        try {
            LiveQuery query = db.getDatabaseInstance().createAllDocumentsQuery().toLiveQuery();
            MedicinePagerAdapter adapter = new MedicinePagerAdapter(MedicineDetailActivity.this,getSupportFragmentManager(), query);
            text.setVisibility(View.GONE);
            mViewPager.setAdapter(adapter);
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    private void fetchAndStoreData() {
        data.put("name", "b");
        data.put("pageSize", "10000000");
        data.put("_", "1435404923427");

        try {
            Call<ListParser> call = App.getRestClient().getMedicineList().getMedicineList(url, data);
            call.enqueue(new Callback<ListParser>() {
                @Override
                public void onResponse(Response<ListParser> apiResponse, Retrofit retrofit) {
                    if (apiResponse.body().getTotalRecordCount() > 0) {
                        for (int i = 0; i < apiResponse.body().getResult().size(); i++) {
                            try {
                            property.put("id", apiResponse.body().getResult().get(i).getId());
                            property.put("packSizeLabel", apiResponse.body().getResult().get(i).getPackSizeLabel());
                            property.put("name", apiResponse.body().getResult().get(i).getName());
                            property.put("oPrice", apiResponse.body().getResult().get(i).getOPrice());
                            property.put("mrp", apiResponse.body().getResult().get(i).getMrp());
                            property.put("drug_form", apiResponse.body().getResult().get(i).getDrugForm());
                            property.put("available", apiResponse.body().getResult().get(i).getAvailable());
                            property.put("manufacturer", apiResponse.body().getResult().get(i).getManufacturer());
                            property.put("prescriptionRequired", apiResponse.body().getResult().get(i).getPrescriptionRequired());
                            property.put("label", apiResponse.body().getResult().get(i).getLabel());
                            property.put("type", apiResponse.body().getResult().get(i).getType());
                            property.put("packSize", apiResponse.body().getResult().get(i).getPackSize());
                                db.addDocument(String.valueOf(apiResponse.body().getResult().get(i).getId()), property);
                            }catch (Exception e){
                            }
                        }
                    }
                    initializeView();
                }

                @Override
                public void onFailure(Throwable t) {
                    Log.e("Error : ", "" + t.getLocalizedMessage());
                    Toast.makeText(MedicineDetailActivity.this , getString(R.string.unable_fetch),Toast.LENGTH_LONG).show();
                }
            });
        }catch(Exception e) {
            Log.e("Crash:" , e.getMessage());
        }
    }

    private int getCount()  {
        Query q = null;
        try {
            q = db.getDatabaseInstance().createAllDocumentsQuery();
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        QueryEnumerator query = null;
        try {
            query = q.run();
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        return query.getCount();
    }
}
