package com.pharmeasy.pharma;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.pharmeasy.pharma.database.CouchBaseDBHelper;

/**
 * Created by kumargaurav on 5/9/16.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(isNetworkAvailable(SplashActivity.this)) {
                    goToMedicineDetail();
                } else {
                    Toast.makeText(SplashActivity.this,getString(R.string.no_network_connection) ,Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }, 5*1000);
    }

    private void goToMedicineDetail() {
        Intent intent = new Intent(SplashActivity.this, MedicineDetailActivity.class);
        startActivity(intent);
        finish();
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

