package com.pharmeasy.pharma.rest.service;


import com.pharmeasy.pharma.rest.model.ListParser;

import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.http.Url;

/**
 * Created by kumargaurav on 2/9/16.
 */
public interface Medicine {
    @GET
    Call<ListParser> getMedicineList(@Url String url, @QueryMap Map<String, String> data);
}
