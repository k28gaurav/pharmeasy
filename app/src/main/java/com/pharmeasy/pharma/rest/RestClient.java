package com.pharmeasy.pharma.rest;

import android.util.Log;

import com.pharmeasy.pharma.rest.service.Medicine;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by kumargaurav on 12/8/15.
 */
public class RestClient
{
    private static final String BASE_URL = "https://www.1mg.com";
    private Medicine apiService;

    public RestClient()
    {
        try {
            Retrofit restAdapter;
            final OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
            okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
            restAdapter = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            apiService = restAdapter.create(Medicine.class);
        }
        catch(Exception e)
        {
        }
    }

    public Medicine getMedicineList()
    {
        return apiService;
    }
}

