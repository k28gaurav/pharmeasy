package com.pharmeasy.pharma.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kumargaurav on 2/9/16.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pharmeasy.pharma.model.Result;


public class ListParser {

    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("errors")
    @Expose
    private Object errors;
    @SerializedName("hasMore")
    @Expose
    private Boolean hasMore;
    @SerializedName("totalRecordCount")
    @Expose
    private Integer totalRecordCount;
    @SerializedName("result")
    @Expose
    private List<Result> result = new ArrayList<Result>();
    @SerializedName("op")
    @Expose
    private String op;
    @SerializedName("suggestions")
    @Expose
    private List<Object> suggestions = new ArrayList<Object>();
    @SerializedName("searchTerm")
    @Expose
    private String searchTerm;
    @SerializedName("status")
    @Expose
    private Integer status;
    /**
     *
     * @return
     * The message
     */
    public Object getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(Object message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The errors
     */
    public Object getErrors() {
        return errors;
    }

    /**
     *
     * @param errors
     * The errors
     */
    public void setErrors(Object errors) {
        this.errors = errors;
    }

    /**
     *
     * @return
     * The hasMore
     */
    public Boolean getHasMore() {
        return hasMore;
    }

    /**
     *
     * @param hasMore
     * The hasMore
     */
    public void setHasMore(Boolean hasMore) {
        this.hasMore = hasMore;
    }

    /**
     *
     * @return
     * The totalRecordCount
     */
    public Integer getTotalRecordCount() {
        return totalRecordCount;
    }

    /**
     *
     * @param totalRecordCount
     * The totalRecordCount
     */
    public void setTotalRecordCount(Integer totalRecordCount) {
        this.totalRecordCount = totalRecordCount;
    }

    /**
     *
     * @return
     * The result
     */
    public List<Result> getResult() {
        return result;
    }

    /**
     *
     * @param result
     * The result
     */
    public void setResult(List<Result> result) {
        this.result = result;
    }

    /**
     *
     * @return
     * The op
     */
    public String getOp() {
        return op;
    }

    /**
     *
     * @param op
     * The op
     */
    public void setOp(String op) {
        this.op = op;
    }

    /**
     *
     * @return
     * The suggestions
     */
    public List<Object> getSuggestions() {
        return suggestions;
    }

    /**
     *
     * @param suggestions
     * The suggestions
     */
    public void setSuggestions(List<Object> suggestions) {
        this.suggestions = suggestions;
    }

    /**
     *
     * @return
     * The searchTerm
     */
    public String getSearchTerm() {
        return searchTerm;
    }

    /**
     *
     * @param searchTerm
     * The searchTerm
     */
    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    /**
     *
     * @return
     * The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }



}
